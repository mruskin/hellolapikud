/**
 * Created by texnika on 24.04.2016.
 */
angular.module('todoListApp')
    .service('dataService', function($http) {

        this.getTodos = function (callback) {
            $http.get('rest/todos.json')
                .then(callback);
        };

        this.deleteTodo = function(todo) {
            console.log("The " + todo.name + " has been deleted!");
            //other db logic
        };

        this.saveTodos = function(todos) {
            console.log(todos.length + " have been saved!");
        };

    });