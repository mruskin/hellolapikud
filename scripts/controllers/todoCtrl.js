/**
 * Created by texnika on 24.04.2016.
 */
angular.module("todoListApp")
    .controller('todoCtrl', function($scope, dataService){ //scope - area kus kontoller töötab
        $scope.ngTest = function() { //htmlis nähtav
            console.log("input changed");
        };

        dataService.getTodos(function(response){
            $scope.todos = response.data;
        });

        $scope.deleteTodo = function(todo, $index) {
            dataService.deleteTodo(todo);
            $scope.todos.splice($index, 1);
        };

        $scope.saveTodos = function(todo, $index) {
           /* var filteredTodos = $scope.todos.filter(function(todo) {
                if(todo.edited) {
                    return todo;
                };
            });*/
            dataService.saveTodos(todo);
        };

        $scope.addTodo  = function() {
            var todo = {name: "new todo"};
            $scope.todos.unshift(todo);
        };
    });