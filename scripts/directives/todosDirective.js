angular.module('todoListApp')
.directive('todos', function() {
    return {
        templateUrl: 'views/todos.html',
        controller: 'todoCtrl'
    }
})